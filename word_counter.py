import logging
import re

from bs4 import BeautifulSoup
from bs4.element import Comment
from nltk.tokenize.regexp import RegexpTokenizer
from scrapy.linkextractors.lxmlhtml import LxmlLinkExtractor
from scrapy.spiders.crawl import CrawlSpider, Rule
from scrapy.utils.log import configure_logging


logger = logging.getLogger('mycustomlogger')
logger.setLevel(logging.INFO)


def is_text_visible(element):
    if element.parent.name in ['meta', 'style', 'script', '[document]', 'head', 'title']:
        return False
    elif isinstance(element, Comment):
        return False
    return True


class WordCounter(CrawlSpider):

    name = "social_quantum"
    allowed_domains = ["www.socialquantum.com"]
    configure_logging(install_root_handler=False)
    logging.basicConfig(
        filename='log.txt',
        format='%(levelname)s: %(message)s',
        level=logging.INFO
    )
    start_urls = ["http://www.socialquantum.com"]
    rules = [Rule(link_extractor=LxmlLinkExtractor(), follow=True, callback='parse_page')]

    def parse_page(self, response):
        parser = BeautifulSoup(response.body, "lxml")
        text = parser.findAll(text=True)
        visible_text = filter(is_text_visible, text)
        actual_text = ([str(t).strip().lower() for t in visible_text if len(str(t).strip()) > 0])
        tokenizer = RegexpTokenizer(r"[\w]+", flags=re.UNICODE)

        words = [tokenizer.tokenize(t) for t in actual_text]

        word_dict = {}
        for word_list in words:
            for w in word_list:
                if w in word_dict:
                    word_dict[w] += 1
                else:
                    word_dict[w] = 1
        return word_dict
