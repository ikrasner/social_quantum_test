from scrapy import signals
from scrapy.crawler import Crawler
from scrapy.utils.project import get_project_settings
from twisted.internet import reactor
from word_counter import WordCounter


class ResultsPipeline(object):

    @staticmethod
    def process_item(item, spider):
        results.append(dict(item))


results = []


def spider_closed(spider):
    reactor.stop()


if __name__ == "__main__":
    settings = get_project_settings()
    settings.overrides['ITEM_PIPELINES'] = {'__main__.ResultsPipeline': 1}
    crawler = Crawler(WordCounter, settings)
    crawler.signals.connect(spider_closed, signal=signals.spider_closed)
    crawler.crawl()
    reactor.run()

    total_result = {}
    for res in results:
        for k, v in res.items():
            if k not in total_result:
                total_result[k] = v
            else:
                total_result[k] += v
    words = filter(lambda w: len(w) > 6, total_result.keys())
    result = max(words, key=lambda w: total_result[w])
    print(result)
